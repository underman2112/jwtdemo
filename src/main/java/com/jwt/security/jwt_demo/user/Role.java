package com.jwt.security.jwt_demo.user;

public enum Role {
    USER,
    ADMIN
}
